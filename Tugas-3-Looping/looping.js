console.log("---Soal no 1---");

console.log('LOOPING PERTAMA')
var loop = 2 ;
while(loop <= 20){
    if(loop%2==0){
        console.log(loop + ' - I love coding');
    }
    loop++
}
console.log('LOOPING KEDUA')
while(loop >= 2){
    if(loop%2==0){
        console.log(loop + ' - I will become a mobile developer');
    }
loop--
}


console.log("---Soal no 2---");

for(var num2=1; num2<=20; num2++)
{
    if(num2%2==0)
    {
        console.log(num2 + " - Berkualitas");
    }else{
        if(num2%3==0)
        {
            console.log(num2 + " - I Love Coding");
        }else{
            console.log(num2 + " - Santai")
        }
    }
}

console.log("---Soal no 3---");

var arrayHash = [];
for(var num3=0; num3<4; num3++)
{
    for(var y=0; y<8;y++)
    {
        arrayHash.push("#");
    }
    console.log(arrayHash.join(""));
    arrayHash = [];
}

console.log("---Soal no 4---");
var arrayHash2 = [];
for(var num4=0; num4<7; num4++)
{
    for(var z=0; z<num4+1;z++)
    {
        arrayHash2.push("#");
    }
    console.log(arrayHash2.join(""));
    arrayHash2 = [];
}

console.log("---Soal no 5---");
var arrayHash3 = [];
for(var num5=0; num5<8; num5++)
{
    for(var x=0; x<8;x++)
    {
        if(x%2==0)
        {
            if(num5%2 == 0)
            {
                arrayHash3.push(" ");
            }else{
                arrayHash3.push("#");
            }
        }else{
            if(num5%2 == 0)
            {
                arrayHash3.push("#");
            }else{
                arrayHash3.push(" ");
            }
        }
    }
    console.log(arrayHash3.join(""));
    arrayHash3 = [];
}
