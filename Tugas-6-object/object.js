console.log("---soal no 1----");
function arrayToObject(arr) {
    var obj = {};
    var name = "";
    var now = new Date()
    var thisYear = now.getFullYear();

    for (var i = 0; i < arr.length; i++){
        name = arr[i][0] + " " + arr[i][1];
        obj.firstname=arr[i][0];
        obj.lastName=arr[i][1];
        obj.gender=arr[i][2];
        if(arr[i][3]!=undefined){
            obj.age = thisYear - Number(arr[i][3]);
        }else{
            obj.age = "Invalid Birth Year";
        }

        console.log(name + " : ");
        console.log(obj);
    }
    
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ];
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2)
arrayToObject(people);

console.log("---Soal no 2---");
function shoppingTime(memberId, money) {
    var obj = {};
    var listPurchased = [];
    var total=0;
    var changeMoney = money;
    var goods = [['Sepatu Stacattu', 1500000],
                ['Baju Zoro', 500000],
                ['Baju H&N ', 250000],
                ['Sweater Uniklooh ', 175000],
                ['Casing Handphone', 50000]
            ];

    if(memberId == ''|| memberId == undefined)
    {
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    }

    for(var z =0;z<goods.length; z++)
    {
        if(changeMoney>=goods[z][1] && changeMoney>0)
        {
            changeMoney -=goods[z][1];
            total+=goods[z][1];
            listPurchased.push(goods[z][0]);
        }
    }

    obj.memberId = memberId,
    obj.money = money,
    obj.listPurchased = listPurchased,
    obj.changeMoney = changeMoney;

    if(listPurchased.length==0)
    {
        return "Mohon maaf, uang tidak cukup";
    }else{
        return obj;
    }

  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
  console.log(shoppingTime('82Ku8Ma742', 170000));
  console.log(shoppingTime('', 2475000));
  console.log(shoppingTime('234JdhweRxa53', 15000));
  console.log(shoppingTime());

console.log("---Soal no 3---");
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var obj = [];

    for(var y = 0; y<arrPenumpang.length; y++)
    {
        var diff = rute.indexOf(arrPenumpang[y][2])-rute.indexOf(arrPenumpang[y][1]);
        obj[y]={};
        obj[y].penumpang = arrPenumpang[y][0];
        obj[y].naikDari = arrPenumpang[y][1];
        obj[y].tujuan = arrPenumpang[y][2];
        obj[y].bayar = 2000*diff;
    }
    
    return obj;

  }
   
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  console.log(naikAngkot([]));
