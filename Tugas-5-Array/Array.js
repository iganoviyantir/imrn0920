console.log("---soal no 1---");

function range(startNum , finishNum)
{
    var start = startNum;
    var end = finishNum;
    var diff= 0;
    var arr = [];
    var isSort = false;

    if(finishNum < startNum)
    {
        start = finishNum;
        end = startNum;
        isSort = true;
    }

    diff = end-start;

    for(var x=0; x<=diff; x++)
    {
        arr[x] = start;
        start++;
    }

    if(arr.length == 0)
    {
        return -1;
    }else{
        if(!isSort){
            return arr;
        }else{
            return arr.sort(function(a, b){return b-a});
        }
    }
}

console.log(range(1, 10))
console.log(range(1))
console.log(range(11,18))
console.log(range(54, 50))
console.log(range())

console.log("---soal no 2---");
function rangeWithStep(startNum , finishNum, step)
{
    var start2 = startNum;
    var end2 = finishNum;
    var diff2= 0;
    var arr2 = [];
    var isStartNumBigger = false;
    var temp= 0;

    diff2 = end2-start2;
    if(diff2<0)
    {
        diff2 *=-1;
        isStartNumBigger = true;
    }

    for(var x=0; x<=diff2; x++)
    {
        if(temp == 0)
        {
            temp = start2;
        }
        if(isStartNumBigger)
        {
            if(end2>temp)
            {
                break;
            }else{
                arr2[x] = temp;
                temp-=step;
            }
        }else{
            if(temp>end2)
            {
                break;
            }else{
                arr2[x] = temp;
                temp+=step;
            }
        }
    }

    if(arr2.length == 0)
    {
        return -1;
    }else{
        return arr2;
    }
}

console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));

console.log("---soal no 3---");
function sum(startNum , finishNum, step)
{
    var start3 = 0;
    var end3 = 0;
    var sumResult=0;
    if(step == undefined)
    {
        step = 1;
    }

    if(startNum==undefined && finishNum==undefined)
    {
        return 0;
    }
    if(startNum!=undefined)
    {
        start3 = startNum;
    }

    if(finishNum!=undefined)
    {
        end3 = finishNum;
    }else{
        return startNum;
    }

    if(finishNum < startNum)
    {
        start3 = finishNum;
        end3 = startNum;
        isSort3 = true;
    }

    for(var x=start3;x<=end3;x+=step)
    {
        sumResult += x;
    }

    return sumResult;
}

console.log(sum(1,10));
console.log(sum(5, 50, 2));
console.log(sum(15,10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum());

console.log("---Soal no 4---");
var input4 = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling()
{
    for(var x=0; x<input4.length;x++)
    {
        console.log("Nomor ID : " + input4[x][0]);
        console.log("Nama Lengkap : " + input4[x][1]);
        console.log("TTL : " + input4[x][2] + " " + input4[x][3]);
        console.log("Hobi : " + input4[x][4]);
        console.log("");
    }
}

dataHandling();

console.log("---Soal no 5---");
function balikKata(str)
{
    var temp="";
    for(var x=str.length-1;x>=0;x--)
    {
        temp +=str.charAt(x);
    }

    return temp;
}

console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode"));
console.log(balikKata("Haji Ijah"));
console.log(balikKata("racecar"));
console.log(balikKata("I am Sanbers"));

console.log("---Soal no 6---");
var input6 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
function dataHandling2(str)
{
    str[1]+=" Elsharawy";
    str[2]= "Provinsi ".concat(str[2]);
    str.splice(4,1);
    str.splice(4,0,"Pria");
    str.splice(5,0,"SMA Internasional Metro");

    var tanggal = str[3].split("/");
    var nama = str[1];
    var month;
    switch(tanggal[1][1].split("0").toString()){
        case "1":month = "Januari";break;
        case "2":month = "Februari";break; 
        case "3":month = "Maret";break; 
        case "4":month = "April";break; 
        case "5":month = "Mei";break; 
        case "6":month = "Juni";break;
        case "7":month = "July";break; 
        case "8":month = "Agustus";break; 
        case "9":month = "September";break; 
        case "10":month = "Oktober";break; 
        case "11":month = "November";break;
        case "12":month = "Desember";break; 
        default:month = "Desember";break; 
    }

    console.log(str);
    console.log(month);
    console.log(tanggal.sort(function (value1, value2) { return value2 - value1 }));
    console.log(str[3].replace(/[/]/g,"-"));
    console.log(nama.slice(0,15));
}

dataHandling2(input6);

