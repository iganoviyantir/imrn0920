console.log("---soal no 1---");

const golden=()=>{
    console.log("this is golden!!")
  }
   
golden();

console.log("---soal no 2---")
const newFunction= (firstName, lastName)=>{
    firstName, lastName
    return console.log(`${firstName} ${lastName}`);
}
   
  //Driver Code 
  newFunction("William", "Imoh");

  console.log("---Soal no 3---");
  let newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

  const {firstName, lastName, destination, occupation} = newObject;

  console.log(firstName, lastName, destination, occupation);

console.log("---Soal no 4---");
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = west.concat(east)
//Driver Code
let combinedArray = [...west, ...east];
console.log(combinedArray);

console.log("---Soal no 5---");
const planet = "earth"
const view = "glass"
var before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
 var after = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor  incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`;
 
// Driver Code
// console.log(before) 
console.log(after);


  