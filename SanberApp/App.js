import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Tugas15 from './Tugas/Quiz3/MainMenuScreen'

export default class App extends Component {
  render(){
    return (
      <View style={{ paddingTop: 20 }}>
        <Tugas15 />
      </View>
    );
  }
}