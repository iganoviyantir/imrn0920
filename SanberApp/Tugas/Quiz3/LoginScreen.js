import React from 'react';
import {
    Platform, View, Text, Image, ScrollView, TextInput, StyleSheet, Button,
    TouchableOpacity, KeyboardAvoidingView
} from 'react-native';

export default function App() {
    return (
        <KeyboardAvoidingView
            behavior={Platform.OS == "ios" ? "padding" : "height"}
            style={styles.container}>
            <ScrollView>
                <View style={styles.containerView}>
                    {/* <Image source={require('./assets/logo.png')} /> */}
                    <Text style={styles.welcomeText}>Welcome Back</Text>
                    <Text style={styles.welcomeText2}>Sign in to continue</Text>
                    <View style={styles.containerForm}>
                        <View style={styles.forminput}>
                            <Text style={styles.formtext}>Email</Text>
                            <TextInput style={styles.input} />
                        </View>
                        <View style={styles.forminput}>
                            <Text style={styles.formtext}>Password</Text>
                            <TextInput style={styles.input} secureTextEntry={true} />
                        </View>
                        <TouchableOpacity style={styles.containerForgotPasword}>
                            <Text style={styles.forgotPassword}>Forgot Password?</Text>
                        </TouchableOpacity>
                        <View style={styles.kotaklogin}>
                            <TouchableOpacity style={styles.btlogin} >
                                <Text style={styles.textbt}>  Sign In </Text>
                            </TouchableOpacity>
                            <Text style={styles.textHaveAnAccount}>-OR-</Text>
                        </View>
                        <View style={styles.containerSosmed}>
                            <View style={styles.btnSosmed}>
                                <Image source={require('./images/001-facebook-1.png')} style={styles.imgIconSosmed}/>
                                <Text style={styles.textSosmed}>Facebook</Text>
                            </View>
                            <View style={styles.btnSosmed}>
                                <Image source={require('./images/003-search.png')} style={styles.imgIconSosmed}/>
                                <Text style={styles.textSosmed}>Google</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </ScrollView>
        </KeyboardAvoidingView>

    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingBottom:100
    },
    containerView: {
        backgroundColor: '#fff',
        marginTop: 20,
        flex: 1
    },
    containerForm: {
        backgroundColor: '#fff',
        alignSelf: 'center',
        width: '85%',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 10,
        elevation: 2,
        borderRadius: 7,
        paddingTop: 30,
        paddingBottom: 30,
        paddingLeft: 20,
        paddingRight: 20,
        marginTop:20
    },
    welcomeText: {
        fontSize: 24,
        color: '#003366',
        textAlign: 'left',
        fontWeight: 'bold',
        marginLeft:'9%' 
    },
    welcomeText2: {
        color: '#4D4D4D',
        marginLeft:'9%' 
    },
    formtext: {
        color: '#4D4D4D'
    },
    containerForgotPasword:{
        alignItems:'right',
        flex:1
    },
    forgotPassword: {
        color: '#000000',
        alignSelf:'end',
        marginHorizontal:30,
        marginVertical:20
    },
    forminput: {
        marginHorizontal: 20,
        marginVertical: 10,
        alignContent: 'center',
        flex: 1
    },
    input: {
        height: 40,
        borderColor: '#E6EAEE',
        borderBottomWidth: 1

    },
    vbutton: {
        marginHorizontal: 90,
        borderRadius: 10,
        marginVertical: 10,
    },
    btlogin: {
        alignItems: "center",
        backgroundColor: "#F77866",
        padding: 13,
        borderRadius: 7,
        marginHorizontal: 30,
        marginBottom: 10,
        flex: 1,
        height:40
    },
    textHaveAnAccount:{
        textAlign:'center',
        marginVertical:15
    },
    textbt: {
        color: 'white',
        textAlign: "center",
        fontSize: 15,
        fontWeight: "bold",
    },
    kotaklogin: {
        marginTop: 20,
        alignContent: 'center',
        flex: 1
    },
    containerSosmed:{
        flexDirection:'row', 
        justifyContent: 'space-between', 
        flex: 0.8, 
        alignSelf:'center'
    },
    btnSosmed:{
        borderColor:"#E6EAEE",
        borderWidth:1,
        flex:0.4,
        borderRadius:7,
        alignItems:'center',
        paddingVertical: 7,
        paddingHorizontal: 10,
        flexDirection: 'row',
        borderRadius: 5

    },
    imgIconSosmed : {
        width:25,
        height:25,
    },
    textSosmed:{
        marginHorizontal:30
    }
});
