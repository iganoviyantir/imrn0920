import React from 'react';
import {
    Platform, View, Text, Image, ScrollView, TextInput, StyleSheet, Button,
    TouchableOpacity, KeyboardAvoidingView
} from 'react-native';

export default function App() {
    return (
        <KeyboardAvoidingView
            behavior={Platform.OS == "ios" ? "padding" : "height"}
            style={styles.container}>
            <ScrollView>
                <View style={styles.containerView}>
                    <View style={styles.searchBox}>
                        <Image source={require('./images/ic_search.png')} style={{width:25, height:25, alignItems:'center',margin:7}}/>
                        <TextInput style={styles.inputSearchBox}></TextInput>
                        <Image source={require('./images/ic_camera.png')} style={{width:25, height:25, alignItems:'center',margin:7}}/>
                    </View>
                </View>
            </ScrollView>
        </KeyboardAvoidingView>

    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingBottom:100
    },
    searchBox:{
        borderWidth:1,
        borderRadius:7,
        borderColor:'#727C8E',
        width:'80%',
        height:40,
        marginHorizontal:10,
        flexDirection: 'row',
    },
    inputSearchBox:{
        flex:1,
        height:38,
        marginRight:20
    }
});
