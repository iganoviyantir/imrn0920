var readline = require('readline');
var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

console.log("---Soal no 1---");

class Score{
    constructor(name, subject, points )
    {
        this.subject = subject;
        this.name = name;
        
        this.nilai = points.split(",");

        var total=0;
        for(var x=0; x<this.nilai.length;x++)
        {
            var num = parseInt(this.nilai[x])
            total +=num;
        }
        this.average = total;
    }

    output(){
        console.log(`Nama : ${this.name}`);
        console.log(`Subject : ${this.subject}`);
        console.log(`Average : ${this.average/3}`);
    }
}

new Score("Iga","React Native", "10,10,7").output();

console.log("---Soal no 2---");
const data = [
    ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
  ]
  
  function viewScores(data, subject) {
    let output = [];
    var pos = 0;
    for(var x=0;x<data[0].length;x++)
    {
        if(data[0][x] === subject)
        {
            pos = x;
        }
    }
    if(pos!=0){
        for(var x=1;x<data.length;x++)
        {
            var object = {
                email : data[x][0],
                subject : subject,
                points : data[x][pos]
            }
            output[x-1] = object;
        }
    }

    if(output.length!=0)
    {
        console.log(output);
    }else{
        console.log("Data not found");
    }
  }

viewScores(data, "quiz-1")
viewScores(data, "quiz - 2")
viewScores(data, "quiz - 3")

console.log("---Soal no 3---");
function recapScores(data) {
    let average = 0;
    let Predikat;
    for(var x=1; x<data.length;x++)
    {
        for(var y=1; y<data[x].length;y++)
        {
            average += data[x][y];
        }

        average/=3;
        if(average>=70 && average<80)
        {
            Predikat="Participant";  
        }else if(average>=80 && average<90){
            Predikat = "Graduate";
        }else if(average>=90)
        {
            Predikat = "Honour";
        }else{
            Predikat = "Below Average";
        }
        
        console.log(`${x}. Email: ${data[x][0]}`);
        console.log(`Rata-rata: ${average}`);
        console.log(`Predikat: ${Predikat}`);
        average = 0;
    }
}
  
  recapScores(data);
