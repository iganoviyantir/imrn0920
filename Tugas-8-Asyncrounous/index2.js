var readBooksPromise = require('./promise.js')
var pos = 0;
var time = 10000;

var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
function listBookIRead(){
    readBooksPromise(time, books[pos])
        .then(function(timeSpent){
            if(pos<books.length){
                pos++;
                time = timeSpent;
                listBookIRead();
            }
        })
        .catch(function(error){
            
        });
}

listBookIRead();