///soal no 1
console.log("---soal no 1---");

var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

console.log(word + " " + second + " " +third + " " +fourth + " " +fifth + " " +sixth + " " +seventh);

///soal no 2
console.log("---soal no 2---");
var sentence = "I am going to be React Native Developer"; 

var firstWord = sentence.substr(0,1);
var secondWord = sentence.substr(2,2);
var thirdWord = sentence.substr(5,5);
var fourthWord = sentence.substr(11,2);
var fifthWord= sentence.substr(14,2);
var sixthWord= sentence.substr(17,5);
var seventhWord= sentence.substr(23,6); 
var eighthWord= sentence.substr(30);

console.log('First Word: ' + firstWord); 
console.log('Second Word: ' + secondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord)

//soal no 3
console.log("---soal no 3---");
var sentence2 = 'wow JavaScript is so cool'; 

var firstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4, 14); 
var thirdWord2 = sentence2.substring(15, 17); 
var fourthWord2 = sentence2.substring(18, 20); 
var fifthWord2= sentence2.substring(21); 

console.log('First Word: ' + firstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);

// soal no 4
console.log("---soal no 4---");
var sentence3 = 'wow JavaScript is so cool'; 

var firstWord3 = sentence3.substring(0, 3); 
var secondWord3 = sentence2.substring(4, 14);
var thirdWord3 = sentence2.substring(15, 17); 
var fourthWord3 = sentence2.substring(18, 20); 
var fifthWord3= sentence2.substring(21);  

var firstWordLength = firstWord3.length  
var secondWordLength = secondWord3.length  
var thirdWordLength = thirdWord3.length  
var fourthWordLength = fourthWord3.length  
var fifthWord3Length = fifthWord3.length  

console.log('First Word: ' + firstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWordLength); 
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWordLength); 
console.log('Fourth Word: ' + fourthWord3+ ', with length: ' + fourthWordLength); 
console.log('Fifth Word: ' + fifthWord3+ ', with length: ' + fifthWord3Length); 